import React from 'react';
import './App.css';
import {AiFillDelete, AiFillPushpin} from "react-icons/ai";
import {Item} from "./utils/utils";


type Props = {
    item: Item;
    deleteClicked: (e: React.MouseEvent<SVGElement>, key: number) => void;
    pinClicked: (e: React.MouseEvent<SVGElement>, key: number) => void;
};

type PinAndDeleteProps = {
    item: Item;
};

const ItemDisplay: React.FC<Props> = (props: Props) => {
    const {item, deleteClicked, pinClicked} = props

    const PinAndDelete: React.FC<PinAndDeleteProps> = (props: PinAndDeleteProps) => {
        const {item} = props
        return (
            <span>
                <AiFillPushpin className={`button ${item.pin ? 'pinned' : ''}`} onClick={(e: React.MouseEvent<SVGElement>) => pinClicked(e, item.key)}/>
                <AiFillDelete className="button" onClick={(e: React.MouseEvent<SVGElement>) => deleteClicked(e, item.key)}/>
            </span>
        )
    }

    switch (item.variant) {
        case "calender":
            const dateTime = new Date(item.date)
            return (
                <div>
                    <div className="grid-container">
                        <h3>{item.title}</h3>
                        <PinAndDelete item={item}/>
                    </div>
                    <h4>{`Invitees: ${item.invitees.toString()}`}</h4>
                    <h4>{`Date and Time: ${dateTime.toUTCString()}`}</h4>
                </div>
            )
        case "contacts":
            const lastContact = new Date(item.last_contact)
            return (
                <div>
                    <div className="grid-container">
                        <h3>{item.name}</h3>
                        <PinAndDelete item={item}/>
                    </div>
                    <h4>{`Company: ${item.company}`}</h4>
                    <h4>{`Emails: ${item.emails.toString()}`}</h4>
                    <h4>{`Phone: ${item.phones.toString()}`}</h4>
                    <h4>{`Last Contact: ${lastContact.toDateString()}`}</h4>
                </div>
            )
        case "dropbox":
            const created = new Date(item.created)
            return (
                <div>
                    <div className="grid-container">
                        <h3>{item.title}</h3>
                        <PinAndDelete item={item}/>
                    </div>
                    <h4>{`Path: ${item.path}`}</h4>
                    <h4>{`Shared With: ${item.shared_with.toString()}`}</h4>
                    <h4>{`Created: ${created.toDateString()}`}</h4>
                </div>
            )
        case "slack":
            const timestamp = new Date(item.timestamp)
            return (
                <div>
                    <div className="grid-container">
                        <h3>{`Author: ${item.author}`}</h3>
                        <PinAndDelete item={item}/>
                    </div>
                    <h4>{`Channel: ${item.channel}`}</h4>
                    <h4>{`Message: ${item.message}`}</h4>
                    <h4>{`Timestamp: ${timestamp.toUTCString()}`}</h4>
                </div>
            )
        case "tweet":
            const date = new Date(item.timestamp)
            return (
                <div>
                    <div className="grid-container">
                        <h3>{item.user}</h3>
                        <PinAndDelete item={item}/>
                    </div>
                    <h4>{`Message: ${item.message}`}</h4>
                    <h4>{`Timestamp: ${date.toDateString()}`}</h4>
                </div>
            )
        default:
            return <div>{item.key}</div>
    }
}

export default ItemDisplay;
