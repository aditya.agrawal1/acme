import calender from "./calendar.json";
import contacts from "./contacts.json";
import dropbox from "./dropbox.json";
import slack from "./slack.json";
import tweet from "./tweet.json";

let finalData: any = [];
let key = 0
finalData.push({"header": "Calender", key: key++})
calender.calendar.forEach((object,index) => finalData.push({...object, variant: "calender", key: key++, pin: false, delete: false}))
finalData.push({"header": "Contacts", key: key++})
contacts.contacts.forEach((object,index) => finalData.push({...object, variant: "contacts", key: key++, pin: false, delete: false}))
finalData.push({"header": "Dropbox", key: key++})
dropbox.dropbox.forEach((object,index) => finalData.push({...object, variant: "dropbox", key: key++, pin: false, delete: false}))
finalData.push({"header": "Slack", key: key++})
slack.slack.forEach((object,index) => finalData.push({...object, variant: "slack", key: key++, pin: false, delete: false}))
finalData.push({"header": "Tweet", key: key++})
tweet.tweet.forEach((object,index) => finalData.push({...object, variant: "tweet", key: key++, pin: false, delete: false}))

export const getFullData = () => {
    return finalData
}

export const updatePinState = (key: number) => {
    finalData[key]["pin"] = !finalData[key]["pin"]
}

export const deleteState = (key: number) => {
    finalData[key]["delete"] = true
}

export type Item = {
    [key: string]: any
}



