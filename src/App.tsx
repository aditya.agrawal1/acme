import React, {useState} from 'react';
import './App.css';
import { getFullData, updatePinState, deleteState } from "./utils/utils"
import ItemDisplay from "./ItemDisplay";
import {Item} from "./utils/utils";

const App = () => {

    const [value, setValue] = useState("")
    const [open, changeOpen] = useState(false)
    const [searchCount, updateSearchCount] = useState(0)
    const [deleteCount, updateDeleteCount] = useState(0)
    const [pinCount, updatePinCount] = useState(0)
    const data = getFullData()

    const pinClicked = async (e: React.MouseEvent<SVGElement>, key: number) => {
        updatePinState(key);
        updatePinCount(data.filter((object: Item) => object.pin).length)
    }

    const deleteClicked = async (e: React.MouseEvent<SVGElement>, key: number) => {
        deleteState(key);
        updateDeleteCount(deleteCount + 1)
    }

    const onChange = (value: string) => {
        setValue(value)
        changeOpen(false)
    }

    const hitSearch = () => {
        changeOpen(true)
        updateSearchCount(searchCount + 1)
    }


    return (
        <div className="App">
            <input onChange={(e:React.ChangeEvent<HTMLInputElement>) => onChange(e.target.value)}/>
            <button onClick={() => hitSearch()} className="search-button">
                Search
            </button>
            <div className="search-result">
                {open && data.map((item: Item) =>
                    !item.delete && (item.header || item.pin || item.matching_terms.includes(value)) ?
                        item.header ?
                            <div
                                className="item-header"
                                key={item.key}
                            >{item.header}</div>
                            : <div
                                className="item"
                                key={item.key}
                            >
                            <ItemDisplay item={item} deleteClicked={deleteClicked} pinClicked={pinClicked}/>
                            </div> :
                        null
                )}
            </div>
            <div className="analytics row">
                <div className="column">{`Search: ${searchCount}`}</div>
                <div className="column">{`Delete: ${deleteCount}`}</div>
                <div className="column">{`Pin: ${pinCount}`}</div>
            </div>
        </div>
    );
}

export default App;
